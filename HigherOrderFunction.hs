-- RANGKUMAN OF ALL HIGHER ORDER FUNCTION


-- Define the length function using map and sum
------------------------------------------
-- No map and sum solution
-- fun (x:xs) = 1 + fun xs
-- fun [] = 0
------------------------------------------
changeToOne a = 1 -- Fungsi yang menerima a dan akan menggantinya menjadi 1
listOfOne list = map changeToOne list -- Membuat semua element menjadi 1 dan disimpan di listOfOne
lenOfList list = sum (listOfOne list) -- menghitung semua angka 1

-- Reference : https://mail.haskell.org/pipermail/hugs-users/2003-July/000427.html
-- ================================================================================================================

-- What does map (+1) (map (+1) xs) do? Can you conclude anything in
-- general about properties of map f (map g xs), where f and g are arbitrary
-- functions?
mapComposition x = map (+1) (map (+1) x)

-- akan menjalankan (map (+1) x) baru kemudian menjalankan map (+1) ({..}), 
-- {..} merupakan list hasil dari (map (+1) x)

-- ================================================================================================================
{-
* Soal : 

iter n f x = f (f (... (f x)))
where f occurs n times on the right-hand side of the equation. For instance, we
should have
iter 3 f x = f (f (f x))
and iter 0 f x should return x.

-}

-- Menggunakan konsep foldr
iter :: (Eq t1, Num t1) => t1 -> (t2 -> t2) -> t2 -> t2

iter 0 f x = x
iter n f x = f (iter (n - 1) f x)

-- Reference : https://wiki.haskell.org/Foldr_Foldl_Foldl'

-- ================================================================================================================
-- (*) How would you define the sum of the squares of the natural numbers 1
-- to n using map and foldr?

sumOfSquares list = foldr (+) 0 (map (^2) list)

-- ================================================================================================================
mystery xs = foldr (++) [] (map sing xs)
    where
        sing x = [x]
{- 
Behavior : 
map sing [1..3] = [[1],[2],[3]]
foldr (++) [] [[1],[2],[3]] = [1,2,3]
pertama lakukan (++) ke [3] dengan [], lalu hasilnya di (++) dengan [2] sampai ke yang paling kiri
-}
    
-- ================================================================================================================

    -- $ untuk mengganti parenthesis "()"
    -- . untuk fungsi komposisi
-- ================================================================================================================
{- 
Define a function composeList which composes a list of functions into a single
function. You should give the type of composeList, and explain why the function
has this type. What is the effect of your function on an empty list of functions?
-}

emptyFun x = x
composeList list = foldr (.) emptyFun list
-- compose semua fungsi di list bernama list, jika list sudah habis, compose dengan fungsi emptyFun yang 
-- menerima parameter x = x. jika list kosong maka hanya akan mereturn fungsi emptyFun

-- ================================================================================================================
{-
• (*) Define the function
• flip :: (a -> b -> c) -> (b -> a -> c)
which reverses the order in which its function argument takes its arguments.
The following example shows the effect of flip:
Prelude> flip div 3 100
33
-}

flipSendiri f a b = f b a

-- ================================================================================================================
-- Can you rewrite the following list comprehensions using the higher-order
-- functions map and filter? You might need the function concat too

-- MAP AND FILTER FROM LIST COMPREHENSION

-- [ x+1 | x <- xs ] [ x+1 | x <- [1,2,3] ] = [2, 3, 4]
oneOne :: Num b => [b] -> [b]
oneOne x = map (+1) x

-- [ x+y | x <- xs, y <-ys ]
-- [1,2] [3, 4] = [4,5,5,6]
twoOne :: Num a => [a] -> [a] -> [a]
twoOne (x:xs) y = map (+x) y ++ twoOne xs y
twoOne [] y = []

-- [ x+2 | x <- xs, x > 3 ]
threeOne list = map (+2) (filter (>3) list)

-- [ x+3 | (x,_) <- xys ]
-- [ x+3 | (x,_) <- [(1,2),(3,4),(5,6)] ] = [4,6,8]
getTupleHead (x, y) = x
fourOne ((a, b) : xs) = map (+3) (map getTupleHead ((a, b) : xs))

-- [ x+4 | (x,y) <- xys, x+y < 5 ]
-- [ x+4 | (x,y) <- [(1,2),(3,4),(5,6),(7,8)], x+y < 5 ] = [5]
addTuple (a, b) = (a + b) < 5
fiveOne ((a, b) : xs) = map (+4) (map (\(x, y) -> a) (filter addTuple ((a, b) : xs)))

-- END OF MAP AND FILTER FROM LIST COMPREHENSION


-- ================================================================================================================
{- Can you it the other way around? I.e. rewrite the following expressions as list
comprehensions.
-}
-- LIST COMPREHENSION FROM MAP AND FILTER

-- map (+3) xs
-- map (+3) [1..10] = [4,5,6,7,8,9,10,11,12,13]
oneTwo xs = [x + 3 | x <- xs]

-- filter (>7) xs
-- filter (>7) [1..10] = [8,9,10]
twoTwo xs = [x | x <- xs, x > 7]

-- concat (map (\x -> map (\y -> (x,y)) ys) xs)
-- concat (  map (\x -> map (\y -> (x,y)) [1..5]) [6..10]  ) = [
--                                                          (6,1),(6,2),(6,3),(6,4),(6,5),(7,1),(7,2),
--                                                          (7,3),(7,4),(7,5),(8,1),(8,2),(8,3),(8,4),
--                                                          (8,5),(9,1),(9,2),(9,3),(9,4),(9,5),(10,1),
--                                                          (10,2),(10,3),(10,4),(10,5)
--                                                          ]
threeTwo = [(y, x)| y <- [6..10], x <- [1..5]]

-- filter (>3) (map (\(x,y) -> x+y) xys)
-- filter (>3) (   map (\(x,y) -> x+y) [(1,2), (3,4)]   ) = [7]
fourTwo = [a + b | (a, b) <- [(1, 2), (3, 4)], a + b > 3 ]
-- END OF LIST COMPREHENSION FROM MAP AND FILTER
-- ================================================================================================================
-- ================================================================================================================
