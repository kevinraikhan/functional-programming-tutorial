-- Expr and evaluate code is from Chapter 07 Haskell school of expression
-- data Expr = C Float 
--           | Expr :+ Expr 
--           | Expr :- Expr
--           | Expr :* Expr 
--           | Expr :/ Expr  deriving Show

-- data Expr = C Float 
--             | Expr :+ Expr 
--             | Expr :- Expr
--             | Expr :* Expr 
--             | Expr :/ Expr
--             | V String
--             | Let String Expr Expr deriving Show

          


-- STUCK di sini, tidak bisa assign evaluate C dan V
-- evaluate (C x)      = x
-- evaluate (V x) = (V x)
-- evaluate (e1 :+ e2) = evaluate e1 + evaluate e2
-- evaluate (e1 :- e2) = evaluate e1 - evaluate e2
-- evaluate (e1 :* e2) = evaluate e1 * evaluate e2
-- evaluate (e1 :/ e2) = evaluate e1 / evaluate e2
-- evaluate (e1 :/ e2) = evaluate e1 / evaluate e2

data Tree a = Leaf a 
    | Branch (Tree a) (Tree a)
    deriving (Show)

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree f (Leaf x) = Leaf (f x)
mapTree f (Branch t1 t2) = Branch (mapTree f t1) (mapTree f t2) 

puhun = Branch ( Branch (Leaf 63) (Leaf 53) ) (Leaf 97)


-- MOCK Expression
e1 = (C 10 :+ (C 8 :/ C 2)) :* (C 7 :- C 4)
e2 = C 10 :+ C 3
{-

1.) Rancanglah, higher order function yang bekerja pada struktur data ekspresi dan memiliki
semantik seperti: Fungsi map pada list, Fungsi fold pada tree

-}
-- Fungsi ini akan bekerja seperti map pada list,
-- Fungsi akan menjalankan fungsi f ke setiap float dari C di Expression


mapExp f (C x) = C (f x)
mapExp f (a :+ b) = mapExp f a :+ mapExp f b
mapExp f (a :- b) = mapExp f a :- mapExp f b
mapExp f (a :* b) = mapExp f a :* mapExp f b
mapExp f (a :/ b) = mapExp f a :/ mapExp f b

-- ===================================================================================================================
{-
2. Pada buku dan contoh sebelumnya, fungsi evaluasi didefinisikan secara rekursif. Gunakan
higher order function yang baru dibuat untuk mendefinisikan fungsi evaluasi tersebut.
Pastikan memiliki makna semantik yang sama dengan definisi sebelumnya
-}

-- #Saya kurang mengerti soal ini

-- ===================================================================================================================
{-
3. Tambahkan konstruksi Let pada struktur data Ekspresi menjadi:
> data Expr = C Float | Expr :+ Expr | Expr :- Expr
> | Expr :* Expr | Expr :/ Expr
> | V String
> | Let String Expr Expr
> deriving Show
-}

-- PAUL HUDAK'S Solution------------------------------------

data Expr = C Float
          | V String
          | Expr :+ Expr
          | Expr :- Expr
          | Expr :* Expr
          | Expr :/ Expr
          | Let String Expr Expr
            deriving Show
 
evaluate :: Expr -> [(String, Float)] -> Float
evaluate (C x) _ = x
evaluate (V x) vars = lookup x vars
    where lookup i [] = error "Unbound variable"
          lookup i ((a,b):vars) = if i == a then b else lookup i vars
evaluate (e1 :+ e2) vars = (evaluate e1 vars) + (evaluate e2 vars)
evaluate (e1 :- e2) vars = (evaluate e1 vars) - (evaluate e2 vars)
evaluate (e1 :* e2) vars = (evaluate e1 vars) * (evaluate e2 vars)
evaluate (e1 :/ e2) vars = (evaluate e1 vars) / (evaluate e2 vars)
evaluate (Let s e1 e2) vars = let es = evaluate e1 vars 
                              in evaluate e2 ((s, es):vars)

-- END OF PAUL HUDAK'S SOLUTION -----------------------------

-- COBA COBA Menggunakan Let
list = []
sesuatu = Let "x" (V "x") (C 30)
hitung = (V "x") :+ (C 20.0)
hasil = evaluate hitung []
nyeh = Let "y" (V "x") (Let "x" (C 2.0) (V "y"))
aslinya = Let "x" (C 3.0) nyeh
asliHasil = evaluate aslinya []

cobaKomen = evaluate (Let "x" (V "y") (Let "y" (C 6.0) (V "x")))

cobaNgerti = Let "x" (C 2.0) (V "x" :+ (C 3.0))
hasilNgertiNih = evaluate cobaNgerti []

-- ===================================================================================================================
    
type Position = (Int,Int)

data Color = Black | Blue | Green | Cyan | Red | Magenta | Yellow | White deriving (Eq, Ord, Bounded, Enum, Show, Read)

data Direction = North | East | South | West deriving (Eq,Show,Enum)

data RobotState = RobotState 
        { position  :: Position
        , facing    :: Direction
        , pen       :: Bool 
        , color     :: Color
        , treasure  :: [Position]
        , pocket    :: Int
        }
    deriving Show

type Robot1 a = RobotState -> (RobotState, a)
-- TAMBAHKAN energi, 

-- ===================================================================================================================
-- ===================================================================================================================
-- ===================================================================================================================
-- ===================================================================================================================
-- ===================================================================================================================
-- ===================================================================================================================
-- ===================================================================================================================