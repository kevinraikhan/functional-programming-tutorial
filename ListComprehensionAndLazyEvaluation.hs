import Data.List
import Debug.Trace
-- -- check x = filter (`mod` 3 == 0) (filter ( `mod` 2 == 1) x)
-- -- test = filter ( `mod` 2) [1..100]
-- -- check :: Integer a => a -> Bool

-- -- check :: Integral a => a -> Bool
-- checkModThree a = mod a 3 == 0
-- checkModTwo a = mod a 2 == 1


-- hasil list = filter checkModTwo (filter checkModThree list)
-- -- result x = map show (hasil x)
-- app a = if a < 10 then (show a) ++ " UNDER 10 " else (show a) ++ " OVER 10 "
-- -- append a = if a < 10 then 99 else 900



{-
Buatlah fungsi divisor yang menerima sebuah bilangan bulat n dan mengembalikan
list bilangan bulat positif yang membagi habis n, Contoh:

LatihanLazy> divisor 12
[1,2,3,4,6,12]
-}
divisor n = [x | x <- [1..n], n `mod` x == 0]
-- ================================================================================================================
{-

Buatlah definisi infinite list untuk permutation.

-}

perms [] = [[]]
perms ls = [ x:ps | x <- ls, ps <- perms (ls \\[x]) ] 

-- ls \\[x] artinya yang ada di ls dan tidak ada di [x]

{-
Misal, perms [1, 2]
1. Ambil element pertama, dicontoh 1
2. jalankan (:) dengan list yang akan didapat dari perms sisanya, dicontoh 1 : perms [2]
3. perms [2] akan mereturn [[2]]
4. jalankan x <- [1], [[2]], hasilnya [[1, 2], ]
5. jalankan lagi namun sekarang x = 2

CONTOH 2
perms [1,2,3] 
STEP 1 = [ x:ps | x <- [1], ps <- perms [1, 2] ]
       = [ x:ps | x <- [1], ps <- [[2,3],[3,2]] ]
       = [[1,2,3],[1,3,2]]
STEP 2 = ..... Lanjutkan seperti step 1
-}
-- ================================================================================================================
{- 

Buatlah definisi infinite list dari triple pythagoras. List tersebut terdiri dari element
triple bilangan bulat positif yang mengikut persamaan pythagoras x^2 + y^2 = z^2.

Contoh:
LatihanLazy > pythaTriple
[(3,4,5),(6,8,10),(5,12,13),(9,12,15),(8,15,17),(12,16,20) … ]
Perhatian urutan penyusun list comprehension nya, coba mulai dari variable z!

-}

pythaTriple = take 10 [(x, y, z) |  z <- [1..], y <- [1..z], x <- [1..y], x^2 + y^2 == z^2]

-- ================================================================================================================
{-
Buatlah definisi untuk memberikan infinite list dari bilangan prima menerapkan
algoritma Sieve of Erastothenes.


In mathematics, the Sieve of Eratosthenes is an ancient algorithm for finding all prime numbers up to any given limit.
 It does so by iteratively marking as composite (i.e., not prime) the multiples of each prime, starting with the first prime number, 2.
  The multiples of a given prime are generated as a sequence of numbers starting from that prime, 
  with constant difference between them that is equal to that prime.[1] This is the sieve's key distinction 
  from using trial division to sequentially test each candidate number for divisibility by each prime.[2]
-}
sieve (p:ps) = p : sieve [ x | x <- ps, x `mod` p /= 0 ]
primes = sieve [2..100]

{-
Misal, List = [2..]
Maka jalankan fungsi sieve dan fungsi tersebut akan mengambil element terdepan, di contoh 2 dan masukan ke list jawaban.
list jawaban sekarang = [2, ]
lalu menjalankan fungsi sieve lagi, kali ini parameternya adalah sisa dari [2..] dan x `mod` p /= 0, 
dalam contoh, parameter list = [3,5,7,...]
lalu fungsi tersebut dijalankan dan mengambil element terdepan dan masukkan ke list jawaban
list jawaban sekarang = [2, 3, ]
lalu menjalankan fungsi sieve lagi, kali ini parameternya adalah sisa dari [2..] dan x `mod` p /= 0, 
dalam contoh, parameter list = [5, 7, 11, ....]
Begitu seterusnya....

-}


    
-- ================================================================================================================
quickSort [] = []
quickSort (x:xs) = quickSort [y | y <- xs,
                                  y <= x] 
                   ++ [x] ++
                   quickSort [y | y <- xs,
                                  y > x]

-- ================================================================================================================
-- ================================================================================================================
-- ================================================================================================================
-- ================================================================================================================
-- ================================================================================================================
-- ================================================================================================================