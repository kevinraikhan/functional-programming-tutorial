{- Can you it the other way around? I.e. rewrite the following expressions as list
comprehensions.
-}

-- map (+3) xs
-- map (+3) [1..10] = [4,5,6,7,8,9,10,11,12,13]
one xs = [x + 3 | x <- xs]

-- filter (>7) xs
-- filter (>7) [1..10] = [8,9,10]
two xs = [x | x <- xs, x > 7]

-- concat (map (\x -> map (\y -> (x,y)) ys) xs)
-- concat (  map (\x -> map (\y -> (x,y)) [1..5]) [6..10]  ) = [
--                                                          (6,1),(6,2),(6,3),(6,4),(6,5),(7,1),(7,2),
--                                                          (7,3),(7,4),(7,5),(8,1),(8,2),(8,3),(8,4),
--                                                          (8,5),(9,1),(9,2),(9,3),(9,4),(9,5),(10,1),
--                                                          (10,2),(10,3),(10,4),(10,5)
--                                                          ]
three = [(y, x)| y <- [6..10], x <- [1..5]]

-- filter (>3) (map (\(x,y) -> x+y) xys)
-- filter (>3) (   map (\(x,y) -> x+y) [(1,2), (3,4)]   ) = [7]
four = [a + b | (a, b) <- [(1, 2), (3, 4)], a + b > 3 ]