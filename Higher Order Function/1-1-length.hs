-- Define the length function using map and sum

-- No map and sum solution
-- fun (x:xs) = 1 + fun xs
-- fun [] = 0


changeToOne a = 1 -- Fungsi yang menerima a dan akan menggantinya menjadi 1
listOfOne = map changeToOne [1, 4, 5, 21, 888, 111, 0, 132] -- Membuat semua element menjadi 1 dan disimpan di listOfOne
result = sum listOfOne -- menghitung semua angka 1

-- Reference : https://mail.haskell.org/pipermail/hugs-users/2003-July/000427.html