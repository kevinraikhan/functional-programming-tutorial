{-
* Soal : 

iter n f x = f (f (... (f x)))
where f occurs n times on the right-hand side of the equation. For instance, we
should have
iter 3 f x = f (f (f x))
and iter 0 f x should return x.

-}
-- Type
iter :: (Eq t1, Num t1) => t1 -> (t2 -> t2) -> t2 -> t2

iter 0 f x = x
iter n f x = f (iter (n - 1) f x)

-- Reference : https://wiki.haskell.org/Foldr_Foldl_Foldl'