-- Can you rewrite the following list comprehensions using the higher-order
-- functions map and filter? You might need the function concat too

-- LIST WITH COMPREHENSION MAP AND FILTER

-- [ x+1 | x <- xs ] [ x+1 | x <- [1,2,3] ] = [2, 3, 4]
one :: Num b => [b] -> [b]
one x = map (+1) x

-- [ x+y | x <- xs, y <-ys ]
-- [1,2] [3, 4] = [4,5,5,6]
two :: Num a => [a] -> [a] -> [a]
two (x:xs) y = map (+x) y ++ two xs y
two [] y = []

-- [ x+2 | x <- xs, x > 3 ]
three list = map (+2) (filter (>3) list)

-- [ x+3 | (x,_) <- xys ]
-- [ x+3 | (x,_) <- [(1,2),(3,4),(5,6)] ] = [4,6,8]
getTupleHead (x, y) = x
four ((a, b) : xs) = map (+3) (map getTupleHead ((a, b) : xs))

-- [ x+4 | (x,y) <- xys, x+y < 5 ]
-- [ x+4 | (x,y) <- [(1,2),(3,4),(5,6),(7,8)], x+y < 5 ] = [5]
addTuple (a, b) = (a + b) < 5
five ((a, b) : xs) = map (+4) (map (\(x, y) -> a) (filter addTuple ((a, b) : xs)))

-- END OF LIST COMPREHENSION WITH MAP AND FILTER